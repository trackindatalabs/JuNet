import tensorflow as tf
import numpy as np

from junet.image import preprocess

'''I'm going to add data deformations in tensorflow in below codes'''
deformation_dict = {}


def base_crop(*dataset, input_size):
    results = []
    for data in dataset:
        data = tf.slice(data, input_size)
        results.append(data)
    return results


def crop_3d(data, coord, size):
    data_shape = data.get_shape().as_list()

    deform_data = tf.slice(data, list(coord) + [0], size + [data_shape[-1]])

    return deform_data


def deformation(dataset, augmentation, grid_n=3, input_shape=(96, 96, 96)):

    data_shape = np.array([256, 256, 256])
    grid_size = (data_shape - np.array(input_shape)) // (grid_n - 1)

    crop_coord = []
    for i in range(len(dataset[0].get_shape().as_list()) - 1):
        crop_coord.append(grid_size[i] * tf.random_uniform([], minval=0, maxval=grid_n, dtype=tf.int64))

    # random_x =
    # random_y = tf.random_uniform([], minval=0, maxval=grid_n, dtype=tf.int64)
    # random_z = tf.random_uniform([], minval=0, maxval=grid_n, dtype=tf.int64)
    # coord = (grid_size[0] * random_x, grid_size[1] * random_y, grid_size[2] * random_z)

    input_data = []
    for data in dataset:
        data = preprocess.image_normalization(data)
        data = crop_3d(data, crop_coord, list(input_shape))
        input_data.append(data)

    return input_data
