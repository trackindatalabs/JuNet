import os
from glob import glob
import zipfile

import sys
import tensorflow as tf
import numpy as np

from junet.data.readers import data_reader
from junet.data import data_manager
from junet.tool.utils import progress_bar


def _int64_feature(value):
    """Wrapper for inserting int64 features into Example proto."""
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def _float_feature(value):
    """Wrapper for inserting float features into Example proto."""
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def _bytes_feature(value):
    """Wrapper for inserting bytes features into Example proto."""
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def _validate_text(text):
    """If text is not str or unicode, then try to convert it to str."""
    if isinstance(text, str):
        return text
    elif isinstance(text, 'unicode'):
        return text.encode('utf8', 'ignore')
    else:
        return str(text)


def data_generator(save_dir, id_list, image_list, label_list, name_classes, weight_list=None, **data_preprocess):
    print("Converting Start TFRecords")
    print("Number of Data :", len(image_list))
    if not len(image_list) == len(label_list) and len(image_list) == len(id_list):
        raise ValueError("Different Length of Image and Label : image %d, label %d, id %d" %
                         (len(image_list), len(label_list), len(id_list)))

    num_classes = len(name_classes)
    label_cnt = {'label_%d' % class_n: 0 for class_n in range(num_classes)}

    for data_n, filename in enumerate(id_list):
        image = data_reader(image_list[data_n])

        label = label_list[data_n]
        if isinstance(label, str):
            if label not in name_classes:
                label = data_reader(label)
            else:
                label = name_classes.index(label)

        if 'data_preprocess' in data_preprocess:
            image, label = data_preprocess['data_preprocess'](image, label)

        # Normalize Data
        image = (image - np.min(image)) / (np.max(image) - np.min(image)) * 255

        # label_onehot = data_manager.onehot_encoding(label, num_classes)

        if label.ndim < 2:
            label_cnt["label_%d" % np.argmax(label)] += 1

        tfrecords_filename = os.path.join(save_dir, filename + '.tfrecords.gz')
        options = tf.python_io.TFRecordOptions(tf.python_io.TFRecordCompressionType.GZIP)
        writer = tf.python_io.TFRecordWriter(path=tfrecords_filename, options=options)

        data_features = {
            'image': _bytes_feature(image.astype(np.uint8).tobytes()),
            'label': _bytes_feature(label.astype(np.uint8).tobytes()),
            'filename': _bytes_feature(str.encode(filename))
        }

        data_dimension = len(image.shape)

        if weight_list is not None:
            binary_weight = (weight_list[data_n] * 255).astype(np.uint8).tobytes()
            data_features['weight_map'] = _bytes_feature(binary_weight)

        string_set = tf.train.Example(features=tf.train.Features(feature=data_features))

        writer.write(string_set.SerializeToString())
        writer.close()

        progress_state = "Generating : %s" % filename
        if label.ndim < 2:
            progress_state += ", Label Count: %s" % str(label_cnt)

        progress_bar(len(id_list), data_n+1, progress_state)

    configuration = {}
    configuration['image_shape'] = list(image.shape)
    configuration['label_shape'] = list(label.shape) if len(label.shape) > 2 else [label.shape[-1]]
    configuration['num_class'] = num_classes
    configuration['num_channel'] = 1
    configuration['data_dimension'] = data_dimension
    configuration['data_list'] = id_list
    configuration['name_classes'] = name_classes
    configuration['label_count'] = label_cnt

    print("\nConverting Finished.")
    sys.stdout.flush()

    return configuration


def write_configuration(config_path, configuration):
    save_dirs = {'log_path': 'log/',
                 'ckpt_dir': 'save_point/',
                 'img_result_dir': 'image/',
                 'tboard_dir': 'tboard/',
                 'result_dir': 'result/'}

    data_list = configuration.pop('data_list')

    with open(config_path, 'w') as save_file:
        # Writing Train Setting
        save_file.write("\n\n[data attributes]")
        for key, value in configuration.items():
            save_file.write("\n%s = %s" % (key, value))
        save_file.write("\ndata_list = %s" % str(data_list))
    return save_file


def get_tfrecords(save_dir, project_title, id_list, image_list, label_list, name_classes, weight_list=None, **data_preprocess):
    project_dir = os.path.join(save_dir, project_title)
    config_path = os.path.join(project_dir, 'configuration' + '.txt')
    tfrecords_dir = os.path.join(project_dir, 'tfrecords/')
    print('Project Title :', project_title)
    if not os.path.exists(project_dir):
        os.makedirs(project_dir)
        os.makedirs(tfrecords_dir)

        print("Start Generating DataSet")
        configuration = data_generator(tfrecords_dir, id_list, image_list, label_list, name_classes, weight_list, **data_preprocess)
        save_file = write_configuration(config_path, configuration)
        # with zipfile.ZipFile(tfrecords_filename, 'a') as zip:
        #     zip.writestr('/configuration.txt', save_file)
    else:
        print("Dataset seems already exists")
    return config_path


if __name__ == "__main__":
    project_title = '0220_SegEngine'
    org_data_path = r'c:/workspace/data/SegEngine/'
    image_dataset = glob(os.path.join(org_data_path, "02_19_Dataset/*/*_MRI.nii"))
    label_dataset = glob(os.path.join(org_data_path, "02_19_Dataset/*/*_Label.nii"))
    id_list = [os.path.basename(path).split('_MRI')[0] for path in image_dataset]
    name_classes = list(range(9))
    get_tfrecords(org_data_path, project_title, id_list, image_dataset, label_dataset, name_classes=name_classes)
